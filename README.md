# KaraokeMugen App JavaScript library
A typed javascript library for accessing a karaokemugen app instance.

Currently I only implemented the functions I need myself over the SocketIO REST API, so some features that work over websocket might not be available at this time. If you need more api functions to be added, feel free to implement and commit them. 

# Installation
`npm install --save karaoke-mugen-app-api` or `yarn add karaoke-mugen-app-api`

Then create an instance of the api service 
```
const mugenApi = new MugenApiRest(url, username, password);
```

# Examples
```
const mugenApi = new MugenApiRest(url, username, password);

// Get specific karas and list all titles
const karas = mugenApi.getKaras({ filter: "renai circulation" });
console.log("Found karas: " + karas.content.map(kara => kara.title).join(", "))

```

# Tests
To run the tests you need an active KaraokeMugen-app running and a testuser.
Run tests with `yarn test` or in watchmode `yarn test --watch`