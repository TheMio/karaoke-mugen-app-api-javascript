// Requires a karaokemugen-app instance to run and a testuser to exist

import { getMugenApiRest, MugenApiRest, TagType } from "../src/MugenApiRest";

let mugenApiRest: MugenApiRest;

beforeAll(async () => {
    mugenApiRest = await getMugenApiRest('http://localhost:1337', 'testuser', 'Testuser');
});

test('loginGuest', async () => {
    const guestMugenApiRest = await getMugenApiRest('http://localhost:1337');
    expect(await guestMugenApiRest.checkAuth()).toEqual(expect.objectContaining({ role: "guest" }));
})

test('login', async () => {
    const karas = await mugenApiRest.checkAuth();
    expect(karas).toEqual(expect.objectContaining({ role: "user" }));
})

test('getKaras', async () => {
    const karas = await mugenApiRest.getKaras({ filter: "circulation" });
    expect(karas).toEqual(
        expect.objectContaining({
            content: expect.arrayContaining(
                [expect.objectContaining({ title: "Renai circulation" })]
            )
        }));
})


test('getTags', async () => {
    const karas = await mugenApiRest.getTags({ type: TagType.LANGUAGE, filter: "Japan" });
    expect(karas).toEqual(
        expect.objectContaining({
            content: expect.arrayContaining([
                expect.objectContaining({
                    i18n: expect.objectContaining({ eng: "Japanese" })
                })
            ]
            )
        }));
})

test('getRepos', async () => {
    const karas = await mugenApiRest.getRepos();
    expect(karas).toEqual(expect.arrayContaining([
        expect.objectContaining({ Enabled: true })
    ]));
})