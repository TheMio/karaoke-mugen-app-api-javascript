import { createReadStream } from 'fs';
import request from 'request';
import requestPromise from 'request-promise';
import { DBPLC } from './types/karaokemugen-app/types/database/playlist';
import { DBTag } from './types/karaokemugen-app/types/database/tag';
import { KaraFileV4, KaraList, MediaInfo, NewKara, OrderParam } from './types/karaokemugen-app/types/kara';
import { RepositoryCommon } from './types/karaokemugen-app/types/repo';
import { Tag } from './types/karaokemugen-app/types/tag';
import { TokenResponse } from './types/karaokemugen-app/types/user';

export { DBTag } from './types/karaokemugen-app/types/database/tag';
export { Kara, KaraList, NewKara, OrderParam } from './types/karaokemugen-app/types/kara';
export { Tag } from './types/karaokemugen-app/types/tag';


export enum TagType {
    SERIES = 1,
    SINGER = 2,
    SONGTYPE = 3,
    CREATOR = 4,
    LANGUAGE = 5,
    KARAAUTHOR = 6,
    COLLECTIONS = 16
}

export class MugenApiRest {

    token: TokenResponse | null | undefined;
    constructor(private serverUrl: string, private username?: string, private password?: string) { }

    init = async () => this.login(this.username, this.password).then(token => this.token = token);

    login = (username?: string, password?: string, securityCode?: number) =>
        this.sendCommand<TokenResponse>(username && password ? 'login' : 'loginGuest', username && password ? {
            username,
            password,
            securityCode
        } : {
            fingerprint: String(Math.random()),
        });

    register = (login: string, password: string, role = 'user', securityCode?: number) =>
        this.sendCommand('createUser', {
            login,
            password,
            role,
            securityCode
        });

    checkAuth = () => this.sendCommand<{
        token: string, // To be used in requests when needed
        username: string,
        role: string, // guest, user or admin (local role)    
    }>('checkAuth');

    uploadFile(filepathLocal: string) {
        return new Promise<{
            originalname: string,
            filename: string,
        }>((success, error) => {
            const req = request.post(this.serverUrl + '/api/importfile', (err, resp, body) => {
                if (err) {
                    error('Error!');
                } else {
                    success(JSON.parse(body));
                }
            });
            req.setHeader('authorization', this.token?.token);
            req.form().append('file', createReadStream(filepathLocal));
        });
    }

    processUploadedMedia = (params: {filename: string, origFilename: string}) => this.sendCommand<MediaInfo>('processUploadedMedia', params)

    getTags = (options: {
        type?: TagType,
        filter?: string,
        from?: number,
        size?: number,
        stripEmpty?: boolean,
    }) => this.sendCommand<{
        infos: {
            count: number,
            from: number,
            to: number
        },
        content: DBTag[]
    }>('getTags', options);

    addTag = (tagObj: Partial<Tag>) => this.sendCommand<Tag>('addTag', tagObj)

    getRepos = () => this.sendCommand<RepositoryCommon[]>('getRepos');

    addRepo = (repo: RepositoryCommon) => this.sendCommand<{ code: number, message: string }>('addRepo', repo)

    getKaras = (options: {
        filter?: string,
        lang?: string,
        from?: number,
        size?: number,
        order?: OrderParam,
        q?: string,
        random?: number,
        blacklist?: boolean
    }) => this.sendCommand<KaraList>('getKaras', options)

    createKara = (properties: KaraFileV4, timeoutMs?: number) => this.sendCommand<NewKara>('createKara', { ...properties }, timeoutMs);

    addKaraToPublicPlaylist = (kids: Array<string>) => this.sendCommand<{ data: DBPLC, code: string }>('addKaraToPublicPlaylist', { kids });

    createUser = (login: string, password: string, role: 'user' | 'admin' = 'user', securityCode?: string
    ) => this.sendCommand<{ code: number, message: string }>('createUser', { login, password, role, securityCode });


    // Generic function to access km api over rest
    sendCommand<T>(cmd: string, body?: any, timeoutMs = 300_000): Promise<T> {
        const requestBody: SocketIOCommand = {
            cmd,
            body: this.token ? { body, authorization: this.token?.token } : { body },
        };
        // console.debug(requestBody);
        return Promise.race([this.timeoutPromise(timeoutMs), requestPromise(this.serverUrl + '/api/command', {
            method: "POST",
            json: true,
            body: requestBody
        })
            .then(r => r as { data: T, error: boolean })
            .then(r => {
                if (r.error) {                    
                    if (!requestBody.body.authorization) {
                        throw new Error("Not logged in! Please call the 'await api.init();' function before using the api. ");
                    }
                    throw r.error;
                }
                return r.data;
            })]);
    }

    timeoutPromise = (timeoutMs: number) => new Promise<any>(function(resolve, reject){
        setTimeout(function() { 
            reject('Time out! Your promise couldnt be fulfilled in half second :c'); 
        }, timeoutMs);
    });
}

interface SocketIOCommand {
    cmd: string,
    body: { body: any, authorization?: string }
}

export async function getMugenApiRest(serverUrl: string, username?: string, password?: string) {
    // Returns a pre-initialized instance with token, ready to use
    const mugenApiRest = new MugenApiRest(serverUrl, username, password);
    await mugenApiRest.init();
    return mugenApiRest;
}