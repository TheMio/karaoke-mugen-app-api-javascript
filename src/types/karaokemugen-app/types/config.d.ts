export type PathType =
	| 'Backgrounds'
	| 'Import'
	| 'Temp'
	| 'SessionExports'
	| 'Previews'
	| 'Avatars'
	| 'Banners'
	| 'StreamFiles'
	| 'BundledBackgrounds'
	| 'DB'
	| 'Bin';
